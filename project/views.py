from django.shortcuts import render
# from project.forms import registerform
from django.contrib.auth import login,authenticate,logout
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth.models import User
from project.models import register,Post,activity,education,followUsers,notify,inbox,likes,postcomment,interest,contact
import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
import random
from django.core.mail import EmailMessage
from django.core import serializers
import json
from datetime import timedelta  
def all_friendz(login_user):
    frndz  = set()  
    req_accept = followUsers.objects.filter(following__user__id = login_user,is_friend=True)|followUsers.objects.filter(followers__user__id = login_user,is_friend=True)
    for i in req_accept:
        frndz.add(i.followers.user.id)
        frndz.add(i.following.user.id)
    ids = list(frndz)
    if login_user in ids:
        ids.remove(login_user)
    frndzz = register.objects.filter(user_id__in=ids)
    return frndzz


# Create your views here.
@login_required
def about(request):
    edu=''
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    nfs = notifications(profile)
    check = education.objects.filter(user=profile)
    fff = len(all_friendz(request.user.id))
    if len(check)>0:
        edu = education.objects.get(user=profile)
    return render(request,'about.html',{'profile':profile,'act':activities,'edu':edu,'fff':fff})

@login_required
def album(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    all = Post.objects.filter(user=profile).order_by('-id')
    nfs = notifications(profile)
    fff = len(all_friendz(request.user.id))


    return render(request,'album.html',{'profile':profile,'act':activities,'all':all,'total':len(nfs),'fff':fff})


def contactview(request):
    if request.method=='POST':
        print(request.POST)
        n = request.POST['name']
        e = request.POST['email']
        c = int(request.POST['number'])
        m = request.POST['message']

        data = contact(name=n,email=e,number=c,message=m)
        data.save()
        context = {'status':'Dear {} Thanks for your feedback'.format(n)}
        return render(request,'contact.html',context)
    return render(request,'contact.html')

@login_required
def dashboard(request):
    userfriendz = all_friendz(request.user.id)
    reg = register.objects.get(user__username=request.user.username)
    all = Post.objects.filter(user__in=userfriendz).order_by('-id')
    comm = postcomment.objects.all()
    nfs = notifications(reg)

    if request.method=='POST':
        if 'postupload' in request.POST:
            cont = request.POST['content']
            post_obj = Post(user=reg,content=cont)
            post_obj.save()
            if 'media' in request.FILES:
                med = request.FILES['media']
                post_obj.media = med
                post_obj.save()
            return HttpResponseRedirect('/project/timeline')
    ls = [i for i in userfriendz if i.is_online==True]

    fff = len(all_friendz(request.user.id))
    

    return render(request,'dashboard.html',{'profile':reg,'all':all,'total':len(nfs),'onlines':ls,'comm':comm,'fff':fff})

def frontpage(request):
    if "username" in request.COOKIES:
        name = request.COOKIES['username']
        user = User.objects.get(username=name)
        login(request,user)
        return HttpResponseRedirect('/project/dashboard')
    if request.method=='POST':    
        if 'signupbtn' in request.POST:
            fsname = request.POST['First-name']
            lsname = request.POST['Last-name']
            email = request.POST['Email']
            password = request.POST['Password']
            dob = request.POST['dob']
            # gender = request.POST['gender']

            user = User.objects.create_user(email,email,password)
            user.first_name = fsname
            user.last_name = lsname
            user.save()

            rg = register(user=user,date_of_birth=dob)
            rg.save()
            msz = 'Dear {} your Account created successfully'.format(fsname)
            # status = 'Password should contain minimum 8 characters, including a lower-case,an upper-case and a digit character.'
            return render(request,'frontpage1.html',{'status':msz})
        if 'forgot' in request.POST:
            uname = request.POST['Email']
            user = User.objects.get(username=uname)
            pwd=request.POST['new']
            user.set_password(pwd)
            user.save()
            return render(request,'frontpage1.html',{'status': 'Pssword Changed Successfully!!'})
        if "loginbtn" in request.POST:
            usname = request.POST['Email']
            pwd = request.POST['Password']
            print('User = ',usname)
            user = authenticate(username=usname,password=pwd)
            if user:
                if user.is_staff:
                    return HttpResponse('<h1>Sorry You are not allowed to login here!!</h1>')
                elif user.is_active:
                    user.last_login=datetime.datetime.now()
                    user.save()
                    login(request,user)

                    pp = register.objects.get(user__username=user)
                    pp.is_online=True
                    pp.save()

                    response= HttpResponseRedirect('/project/dashboard')
                    if "rememberme" in request.POST: 
                        response.set_cookie('username',usname)
                        response.set_cookie('id',user.id)
                        response.set_cookie('logintime',datetime.datetime.now())
                    return response
                
            else:
                return render(request,'frontpage1.html',{'status': 'Invalid User Details'}) 
        
    return render(request,'frontpage1.html')

@login_required
def settings_account(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    nfs = notifications(profile)
    fff = len(all_friendz(request.user.id))


    return render(request,'settings_account.html',{'profile':profile,'all':all,'act':activities,'total':len(nfs),'fff':fff})

@login_required
def settings_basicinfo(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    nfs = notifications(profile)
    fff = len(all_friendz(request.user.id))

    if request.method == "POST":
        fs = request.POST['first']
        ls = request.POST['last']
        em = request.POST['email']
        # us = request.POST['username']
        cont = request.POST['contact']
        
        dob = request.POST['date']
        pro = request.POST['prof']
        abt = request.POST['about']
        # cont = request.POST['country']
        # stat = request.POST['state']
        # ct = request.POST['city']
        user.first_name=fs
        user.last_name =ls
        user.email = em
        # user.username = us
        user.save()
        profile.contact = cont
        profile.date_of_birth=dob
        profile.profession=pro
        profile.about=abt
        # profile.country=cont
        # profile.state=stat
        # profile.city=ct
        profile.save()
        if 'optradio' in request.POST:
            gen = request.POST['optradio']
            profile.gender=gen
            profile.save()

        if "image" in request.FILES:
            img = request.FILES['image']
            profile.profile_pic = img
            profile.save()
        
        if "image1" in request.FILES:
            img1 = request.FILES['image1']
            profile.cover_pic = img1
            profile.save()

        return render(request,'settings_basicinfo.html',{'data':user,'profile':profile,'status':'Changes Saved Successfully!!','c':'success'})
    return render(request,'settings_basicinfo.html',{'profile':profile,'act':activities,'total':len(nfs),'fff':fff})

@login_required
def settings_education(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    edu=''
    check = education.objects.filter(user=profile)
    nfs = notifications(profile)
    fff = len(all_friendz(request.user.id))

    if len(check)>=1:
        edu = education.objects.get(user=profile)
    else:
        edu = education(user=profile)
    if request.method == "POST":
        comp = request.POST['company']
        pos = request.POST['position']
        w_city = request.POST['work_c']
        w_from = request.POST['work_f']
        w_to = request.POST['work_t']
        uni = request.POST['university']
        u_city = request.POST['uni_c']
        u_from = request.POST['uni_f']
        u_to = request.POST['uni_t']
        streem = request.POST['stream']
        sch = request.POST['school']
        s_city = request.POST['school_c']
        s_from = request.POST['school_f']
        s_to = request.POST['school_t']
        
        edu.save()
        edu.company = comp
        edu.position = pos
        edu.work_city = w_city
        edu.work_from = w_from
        edu.work_to = w_to
        edu.university = uni
        edu.uni_city = u_city
        edu.uni_from = u_from
        edu.uni_to = u_to
        edu.stream = streem
        edu.school = sch
        edu.school_city = s_city
        edu.school_from = s_from
        edu.school_to = s_to
        edu.save()
        return render(request,'settings_education.html', {'profile':profile,'status':'Changes Saved Successfully!!','c':'success'})
    return render(request,'settings_education.html', {'profile':profile,'act':activities,'edu':edu,'total':len(nfs),'fff':fff})

@login_required
def settings_interests(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    nfs = notifications(profile)
    inter = interest.objects.filter(user=profile)
    fff = len(all_friendz(request.user.id))
    if 'id' in request.GET:
        id=request.GET['id']
        inter = interest.objects.filter(id=id)
        inter.delete()
        return HttpResponseRedirect('/project/s_interest')
    if request.method=='POST':
        int_add = request.POST['interest']
        intrust = interest(user=profile,add_interest=int_add)
        intrust.save()

        return render(request,'settings_interests.html',{'profile':profile,'act':activities,'total':len(nfs),'interest':inter})

    return render(request,'settings_interests.html',{'profile':profile,'act':activities,'total':len(nfs),'interest':inter,'fff':fff})

@login_required
def settings_password(request):
    user = User.objects.get(username=request.user.username)
    login_user_password = request.user.password
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    nfs = notifications(profile)
    fff = len(all_friendz(request.user.id))
    if request.method == 'POST':
        current = request.POST['old']
        newpas = request.POST['new']

        check = check_password(current,login_user_password)
        if check==True:
            user.set_password(newpas)
            user.save()
            return render(request,'settings_password.html',{'status':'Password Change Successfully!!','col':'success'})
        else:
            return render(request,'settings_password.html',{'status':'Incorrect Current Password!!','col':'danger'})
    return render(request,'settings_password.html',{'profile':profile,'act':activities,'total':len(nfs),'fff':fff})

def uslogin(request):
    return render(request,'frontpage1.html')

@login_required
def uslogout(request):
    user = User.objects.get(id=request.user.id)
    user.last_login = datetime.datetime.now()
    user.save()
    logout(request)

    pp = register.objects.get(user__username=user)
    pp.is_online=False
    pp.save()
    
    response = HttpResponseRedirect('/')
    response.delete_cookie('notifications')
    response.delete_cookie('id')
    response.delete_cookie('username')
    response.delete_cookie('logintime')
    return response

def notifications(id):
    data = notify.objects.filter(user_id=id,status=False)
    return data


@login_required
def timeline(request):
    reg = register.objects.get(user__username=request.user.username)
    all = Post.objects.filter(user=reg).order_by('-id')
    activities = activity.objects.filter(user=reg).order_by('-id')[:5]
    nfs = notifications(reg)
    fff = len(all_friendz(request.user.id))
    if request.method == 'POST':
        if 'postupload' in request.POST:
            cont = request.POST['content']
            post_obj = Post(user=reg,content=cont)
            post_obj.save()
            if 'media' in request.FILES:
                med = request.FILES['media']
                post_obj.media = med
                post_obj.save()
            act = "{} has upload a post".format(reg.user.first_name)
            # like = "{} has liked a Photo".format(reg.user.first_name)
            # comment = "{} has commented on Photo".format(reg.user.first_name)
            obj = activity(user=reg,content=act)
            obj.save()
            return HttpResponseRedirect('/project/timeline')

    return render(request,'timeline.html',{'profile':reg,'all':all,'act':activities,'total':len(nfs),'fff':fff})

@login_required
def find_friend(request):
    pfile = register.objects.get(user__username=request.user.username)
    friendz=all_friendz(request.user.id)
    f_ids=[]
    for i in friendz:
        f_ids.append(i.id)
    f_ids.append(pfile.id)

    reg = register.objects.exclude(id__in=f_ids)
    nfs = notifications(pfile)

    userfriendz = all_friendz(request.user.id)
    ls = [i for i in userfriendz if i.is_online==True]

    if request.method=="POST":
        to_search = request.POST['content']
        profile = register.objects.filter(user__first_name__contains=to_search)
        return render(request,'find friend.html',{'users':profile,'profile':pfile})
    return render(request,'find friend.html',{'users':reg,'profile':pfile,'total':len(nfs),'onlines':ls})

def friends(request):
    user = User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    login_user = request.user.id
    nfs = notifications(profile)
    fff = len(all_friendz(request.user.id))
    return render(request,'friends.html',{'profile':profile,'req':all_friendz(login_user),'total':len(nfs),'fff':fff})


def friend(request):
    user=User.objects.get(username=request.user.username)
    login_user = request.user.id
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    nfs = notifications(profile)
    fff = len(all_friendz(request.user.id))


    return render(request,'friend.html',{'profile':profile,'act':activities,'req':all_friendz(login_user),'total':len(nfs),'fff':fff})  

def follow(request):
    login_user = request.user.id
    request_user = request.GET['id']
    follows = register.objects.get(user__id = login_user) 
    followi = register.objects.get(user__id = request_user) 
    
    check = followUsers.objects.filter(followers=follows,following=followi)
    checkfriend = followUsers.objects.filter(followers=follows,following=followi,is_friend=True)
    if len(check)>=1:
        req = followUsers.objects.get(followers=follows,following=followi)
        req.delete()
        return HttpResponse('Add Friend')
    else:
        req = followUsers(followers=follows,following=followi)
        req.save()
        cont = "{} {} send you a friend request".format(request.user.first_name,request.user.last_name)
        note = notify(user=followi,frm=follows,content=cont)
   
        note.save()
        return HttpResponse('Cancel Request')

def friendRequest(request):
    login_user = request.user.id
    req_accept = followUsers.objects.filter(following__user__id = login_user,is_friend=False)
    profile = register.objects.get(user__username=request.user.username)
    nfs = notifications(profile)

    userfriendz = all_friendz(request.user.id)
    ls = [i for i in userfriendz if i.is_online==True]


 
    return render(request,'friend_req.html',{'req':req_accept,'total':len(nfs),'onlines':ls})

def ConfDel(request):
    uid = request.GET['id']
    ac = request.GET['ac']
    foll = followUsers.objects.get(id=uid)
    if ac=='con':
        foll.is_friend=True
        foll.save()
        return HttpResponse(1)

    if ac=='dl':
        foll.delete()
        return HttpResponse(0)

def convert_time_ago(d):
    now=datetime.datetime.now()
    st=str(d+timedelta(minutes=330)).split('+')[0]
    lg=datetime.datetime.strptime(st, '%Y-%m-%d %H:%M:%S.%f')
    active=timeago.format(lg, now)   
    return active

def messages(request):
    ls = []
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    nfs = notifications(profile)
    fff = len(all_friendz(request.user.id))
    allm = set()
    mszs = inbox.objects.filter(sender=profile)|inbox.objects.filter(receiver=profile).order_by('-id')
    for i in mszs:
        allm.add(i.sender.id)
        allm.add(i.receiver.id)
    rm_login = list(allm)
    if profile.id in rm_login:
        rm_login.remove(profile.id)
    users = register.objects.filter(id__in=rm_login)
    for i in users:
        msz = {}
        msz['user']=i
        ddd = inbox.objects.filter(receiver=profile,sender=i).order_by('-id')|inbox.objects.filter(sender=profile,receiver=i).order_by('-id')
        msz['last'] = ddd[0].message
        msz['ctime'] = convert_time_ago(ddd[0].created_at)
        msz['unread'] = len(inbox.objects.filter(sender=i,receiver=profile,status=False))
        ls.append(msz)
    
    return render(request,'messages.html',{'profile':profile,'total':len(nfs),'messages':ls,'fff':fff})

def notification(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    nfs = notify.objects.filter(user=profile).order_by('-id')
    total = len(nfs)

    userfriendz = all_friendz(request.user.id)
    ls = [i for i in userfriendz if i.is_online==True]
    fff = len(all_friendz(request.user.id))
    
    return render(request,'notification.html',{'all':nfs,'totaln':total,'profile':profile,'onlines':ls,'fff':fff})

def forgot_password(request):
    uname = request.GET['Email']
    check = User.objects.filter(username=uname)
    
        
    if len(check)>=1:
        em = User.objects.get(username=uname)
        email = em.email
        otp = random.randint(10000,99999)
        msz = "Hii {}  Here is your OTP {}.".format(uname,otp)

        em = EmailMessage('OTP Verification',msz,to=[email,])
        em.send()
        return HttpResponse('An OTP Sent To Your Registered Email ID @'+str(otp))
    else:
        return HttpResponse('User With this email is not found!!')

def checkn(request):
    tt = request.COOKIES['notifications']
    return HttpResponse(tt)

def hide_notification(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    hideid = request.GET['id']
    all_noti = notify.objects.filter(user=profile,status=False)


    for i in all_noti:
        i.status = True
        i.save()
    res =  HttpResponse(len(all_noti))
    res.set_cookie('notifications',len(all_noti))
    return res

def mark_as_read(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    rc = request.GET['rcvr']
    sn = request.GET['sndr']
    rcvr = register.objects.get(user__id=rc)
    sndr = register.objects.get(user__id=sn)
    unreads = inbox.objects.filter(receiver=rcvr)|inbox.objects.filter(receiver=sndr)
    for i in unreads:
        print('i==',i)
        i.status = True
        i.save()
    return HttpResponse(len(unreads))

import timeago
def user_inbox(request):
    receiver = request.GET['id']
    rec= register.objects.get(user__id=receiver)
    active=convert_time_ago(rec.user.last_login)
    # age=convert_time_ago(rec.date_of_birth)
    age=''
    return render(request,'inbox.html',{'det':rec,'active':active,'age':age})

def save_message_ajax(request):
    if request.method == "POST":
        sndr = request.user.id
        rcvr = request.POST.get('rcvr')

        sender = register.objects.get(user__id=sndr)
        receiver = register.objects.get(user__id=rcvr)
        message = request.POST.get('message')
        message_detail = inbox(sender=sender, receiver=receiver,message=message)
        message_detail.save()
        return HttpResponse("Success")
    return HttpResponse("failure")

def show_message_all(request):
    if request.method == "POST":
        rc = request.POST.get('rcvr')
        sn = request.POST.get('sndr')
        rcvr = register.objects.get(user__id=rc)
        sndr = register.objects.get(user__id=sn)
        sent_mszs = inbox.objects.filter(sender=sndr,receiver=rcvr)|inbox.objects.filter(sender=rcvr,receiver=sndr).order_by('-created_at')
        serialized_obj = serializers.serialize('json',sent_mszs)
        return HttpResponse(serialized_obj, content_type = 'application/json')

def ajax_fun(request):
    if request.method == "POST":
        if request.POST.get('usernamefromid'):
            uid = request.POST.get('usernamefromid')
            reg = register.objects.get(id=uid)
            data = {'name':reg.user.first_name+' '+reg.user.last_name,'pic':str(reg.profile_pic)}
            n = json.dumps(data)
            return HttpResponse(n, content_type="application/json")

def like_unlike(request):
    postid = request.GET['pid']
    userid = request.GET['uid']

    getuser = register.objects.get(user__id=userid)
    getpost = Post.objects.get(id=postid)
    touser = register.objects.get(id=getpost.user.id)
    check = likes.objects.filter(follower=getuser,postid=getpost)
    
    if len(check)==0:
        data = likes(follower=getuser,postid=getpost)
        data.save()
        content =  '{} has liked your Post'.format(getuser.user.first_name+' '+getuser.user.last_name)
        nt = notify(frm=getuser,user=touser,content=content)
        nt.save()
        all = len(likes.objects.filter(postid=getpost))
        res = {'all':all,'action':'Liked'}
        n=json.dumps(res)
        return HttpResponse(n, content_type="application/json")
    else:
        d=likes.objects.get(follower=getuser,postid=getpost)
        d.delete()
        all = len(likes.objects.filter(postid=getpost))
        res = {'all':all,'action':'Removed'}
        n=json.dumps(res)
        return HttpResponse(n, content_type="application/json")
        
    return HttpResponse(len(check))

def view_likes(request):
    postid = request.GET['pid']
    getpost = Post.objects.get(id=postid)
    comm = likes.objects.filter(postid=getpost)
    check = likes.objects.filter(postid=getpost,follower__user__id=request.user.id)
    color='text-secondary'
    if len(check)>=1:
        color='text-primary'
    res = {'comm':len(comm),'color':color}
    n=json.dumps(res)
    return HttpResponse(n, content_type="application/json")

def comments(request):
    if request.method=="POST":
        pid = request.POST['postid']
        uid = request.POST['userid']
        cm = request.POST['comment']

        getp = Post.objects.get(id=pid)
        getu = register.objects.get(user__id=uid)
        d = postcomment(postid=getp,commenterid=getu,comment=cm)
        d.save()
        return  HttpResponseRedirect('/project/dashboard')

def check_user(request):
    un = request.GET['username']
    check = User.objects.filter(username=un)
    if len(check)!=0:
        return HttpResponse("A user with this email already exists!!")
    else:
        return HttpResponse("")